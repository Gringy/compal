canvas = document.getElementById "canvas"
ctx = canvas.getContext "2d"

w = canvas.width = canvas.clientWidth
h = canvas.height = canvas.clientHeight

ctx.font = "10px monospace"
ctx.translate 20, 20

randInt = (min, max) ->
	Math.floor do Math.random * (max-min) + min

params =
	drawNums: false
	drawLines: false
	findLongest: false
	greenway: true

Array.prototype.mix = () ->
	mas = @[..]
	ret = []
	for i in [0...@.length]
		index = randInt 0, (@.length-i-1)
		elem = mas[index]
		mas.splice (index), 1
		ret.push elem
	return ret

class Point
	constructor: (@x, @y) ->
		@roads = []
	dist: (other) ->
		Math.sqrt (@x - other.x)*(@x - other.x) + (@y - other.y)*(@y - other.y)
	getSomeRoad: (to, pref) ->
		if do Math.random < pref
			@roads[randInt 0, @roads.length]
		else
			nr = @roads.sort (a, b) -> a.to.dist(to) - b.to.dist(to)
			nr[0]
	isRoadTo: (point) ->
		for road in @roads when road.to is point then return true
		return false

class Road
	constructor: (@from, @to, @speed) ->
	time: () -> (@from.dist @to) / @speed
	getOther: (point) ->
		if point == @from
			return @to
		else if point == @to
			return from
		else throw new Error("something bad with ways")
	eq: (road) ->
		if road.from == @from and road.to == @to || road.from ==@to and road.to == @from
			return true
		else return false

class Map
	constructor: (nx, ny, param) ->
		@points = []
		if not ny? then ny = nx
		for x in [0...nx]
			for y in [0...ny] 
				if do Math.random < .2 then continue
				dx = randInt -5, 5
				dy = randInt -5, 5
				@points.push new Point x*20+ dx, y*20 + dy
		p = @points.length
		@src = @points[randInt 0, 10]
		@dst = @points[randInt p-10, p]
		for point in @points
			nearest = []
			for npoint in @points
				if npoint == point then continue
				nearest.push npoint
				if nearest.length == 4
					nearest = nearest.sort (a, b) -> a.dist(point) - b.dist(point)
					do nearest.pop
			for npoint in nearest when not (npoint.isRoadTo point) and not (point.isRoadTo npoint)
				if param is "greenway"
					speed = 1
				else
					speed = randInt 1, 255
				point.roads.push new Road point, npoint, speed
				npoint.roads.push new Road npoint, point, speed
		if param == "greenway"
			way = new Way "rand", @, .8
			for road in way.way
				road.speed = 255
				for road2 in road.to.roads when road2.to == road.from
					road2.speed = 255
	draw: () ->
		ctx.fillStyle = "black"
		ctx.fillRect -20, -20, w, h
		ctx.strokeStyle = "green"
		for point in @points
			for road in point.roads
				do ctx.beginPath
				ctx.strokeStyle = "rgb(#{255-road.speed},#{road.speed},0)"
				ctx.moveTo road.from.x, road.from.y
				ctx.lineTo road.to.x, road.to.y
				do ctx.stroke
		ctx.fillStyle = "blue"
		for point in @points
			ctx.fillRect point.x-2, point.y-2, 3, 3
		ctx.fillStyle = "white"
		ctx.fillRect @src.x-4, @src.y-4, 7, 7
		ctx.fillStyle = "yellow"
		ctx.fillRect @dst.x-4, @dst.y-4, 7, 7

class Way
	constructor: (type, map, arg1, arg2) ->
		@way = []
		switch type
			when "rand"
				until @rand map, arg1 then
			when "cross"
				@cross arg1, arg2
			when "mutate"
				@mutate arg1, arg2
			when "empty"
				return
	rand: (map, pref) ->
		way = []
		curr = map.src
		for i in [0...4000]
			currroad = curr.getSomeRoad(map.dst, pref)
			f = off
			for road in way
				if currroad.to == road.to or currroad.to == road.from
					f = on
			if f 
				a = way.pop()
				curr = a.from
				continue
			way.push currroad
			curr = currroad.to
			if curr == map.dst then break
		if curr == map.dst
			@way = way
			return true
		return false
	cross: (from1, from2) ->
		if from1.eq from2 then return
		crossRoads = []
		for road in from1.way
			for nroad in from2.way
				if road == nroad then crossRoads.push road
		if crossRoads.length == 0 then return
		way = []
		cross = crossRoads[randInt 0, crossRoads.length]
		if Math.random < .5 then [from1, from2] = [from2, from1]
		for road in from1.way
			way.push road
			if road == cross then break
		f = off
		for road in from2.way
			if not f and road == cross then f = on; continue
			if f then way.push road
		@way = way
		if (@eq from1) or (@eq from2)
			@way = []
	mutate: (from, pref) ->
		n = randInt 1, from.way.length - 3
		way = from.way[0...n]
		postWay = from.way[n+1..]
		src = from.way[n].from
		nway = []
		# тестит, что путь не повторяется
		isLooped = () ->
			for i in [0...nway.length]
				for j in [i+1...nway.length]
					if nway[i].from is nway[j].to
						return true
			return false
		# тестит, что путь пришёл к оставшемуся и не слишком короткий
		isFound = () -> 
			if nway.length < 2 then return false
			end = nway[nway.length-1]
			for road in postWay
				if road.from is end.to then return true
			return false
		f = (r) ->
			if r is 0 then return false
			if do isFound then return true
			for road in nway[nway.length-1].to.roads.mix()
				nway.push road
				if not do isLooped
					if f r-1 then return true
				do nway.pop
		for road in src.roads.mix()
			nway.push road
			if f pref then break
			do nway.pop
		if nway.length is 0 then return
		way = way.concat nway
		flag = false
		for road in postWay
			if road.from == nway[nway.length-1].to
				flag = true
			if flag then way.push road
		@way = way
		#do @isValid
		if @eq from then @way = []
	time: () ->
		if @_time? then return @_time
		ret = 0
		for road in @way
			ret += do road.time
		@_time = ret
		return ret
	draw: () ->
		ctx.strokeStyle = "yellow"
		ctx.fillStyle = "white"
		ctx.lineWidth = 4
		do ctx.beginPath
		for way, index in @way
			ctx.moveTo way.from.x, way.from.y
			ctx.lineTo way.to.x, way.to.y
			if params.drawNums
				ctx.fillText "#{index}", way.to.x, way.to.y
		do ctx.stroke
		if params.drawLines
			ctx.lineWidth = 2
			for road in @way
				do ctx.beginPath
				ctx.strokeStyle = "rgb(#{255-road.speed},#{road.speed},0)"
				ctx.moveTo road.from.x, road.from.y
				ctx.lineTo road.to.x, road.to.y
				do ctx.stroke
		ctx.lineWidth = 1
	isValid: () ->
		curr = @way[0]
		for road in @way[1..]
			if road.from isnt curr.to
				throw new Error("lol")
			curr = road
	eq: (other) -> 
		if @way.length isnt other.way.length then return false
		for i in [0...@way.length] when @way[i] isnt other.way[i] then return false
		return true

class Alg
	constructor: (type) ->
		if params.greenway
			@map = new Map 40, 30, "greenway"
		else
			@map = new Map 40, 30
		@generation = 0
		@population = []
		for i in [130..255] by 1
			@population.push new Way "rand", @map, i/255
		@current = 0
		@kill = 40
		@mutate = 100
		@addmutants = 1
		do @draw
	draw: () -> 
		if @current >= @population.length
			@current = 0
		if @current < 0
			@current = @population.length - 1
		do @map.draw
		document.getElementById "gen"
			.innerHTML = "Поколение:#{@generation}<br>
				Размер популяции:#{@population.length}<br>
				Текущая особь:#{@current}<br>
				Время особи:#{(Math.floor @population[@current].time()*100)/100}"
		do @population[@current].draw
	killSome: () ->
		if @population.length <= @kill then return
		@population = @population.slice 0, @kill
	crossSome: () ->
		l = @population.length
		for way1, i1 in @population
			for way2, i2 in @population when way1 isnt way2 and do Math.random < (2*l - i1 - i2) / l / 2
				crossed = new Way "cross", @map, way1, way2
				if crossed.way.length != 0
					@population.push crossed
		return
	mutateSome: () ->
		while @population.length <= @mutate then for way in @population
			mutated = new Way "mutate", @map, way, (randInt 4, 30)
			if mutated.way.length isnt 0
				@population.push mutated
	addMutants: () ->
		for i in [0...@addmutants]
			@population.push new Way "rand", @map, (randInt 130, 255)
	tick: () ->
		@generation++
		if params.findLongest
			@population.sort (a, b) -> b.time() - a.time()
		else
			@population.sort (a, b) -> a.time() - b.time()
		do @killSome
		do @addMutants
		do @crossSome
		do @mutateSome

alg = new Alg
timer = null

document.getElementById "go"
	.addEventListener "click", (e) ->
		timer = if not timer? then setInterval ->
			do alg.tick
			do alg.draw
		, 500 else 
			clearInterval timer
			null
		return

document.getElementById "regen"
	.addEventListener "click", (e) ->
		alg = new Alg
document.getElementById "+"
	.addEventListener "click", (e) ->
		alg.current++
		do alg.draw
document.getElementById "-"
	.addEventListener "click", (e) ->
		alg.current--
		do alg.draw
document.getElementById "colors"
	.addEventListener "change", (e) ->
		params.drawLines = e.target.checked
		do alg.draw
document.getElementById "numbers"
	.addEventListener "change", (e) ->
		params.drawNums = e.target.checked
		do alg.draw
document.getElementById "long"
	.addEventListener "change", (e) ->
		params.findLongest = e.target.checked
		do alg.draw
document.getElementById "fast"
	.addEventListener "change", (e) ->
		params.greenway = e.target.checked
		alg = new Alg
		do alg.draw
document.getElementById "kill"
	.addEventListener "change", (e) ->
		val = +e.target.value
		if not isNaN val
			alg.kill = val
		else e.target.value = alg.kill
document.getElementById "addmutants"
	.addEventListener "change", (e) ->
		val = +e.target.value
		if not isNaN val
			alg.addmutants = val
		else e.target.value = alg.addmutants
document.getElementById "mutate"
	.addEventListener "change", (e) ->
		val = +e.target.value
		if not isNaN val
			alg.mutate = val
		else e.target.value = alg.mutate